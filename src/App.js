import Table from './components/Table/Table';
import TopNavigation from './components/TopNavigation/TopNavigation';
import SideNavigation from './components/SideNavigation/SideNavigation';
import Message from './components/Message';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { useState } from 'react'
import './styles/styles.scss';

function App() {

  // Mobile toggle btn for top navbar
  const [topnavtoggle, settopnavtoggle] = useState(false);

  // Mobile toggle btn for side navbar
  const [sidenavtoggle, setsidenavtoggle] = useState(false);

  return (
    <div className="dashboard-body">
      
      <Grid container>

        <Grid item xs={12}>
          
          <Paper>
          
            <TopNavigation 

              // trigger mobile top navbar
              topNavToggle={settopnavtoggle}
              topNavState={topnavtoggle}
              
              // trigger mobile side navbar
              sideNavToggle={setsidenavtoggle}
              sideNavState={sidenavtoggle}

            />

          </Paper>

        </Grid>

        <Grid item md={sidenavtoggle ? 1 : false}>

          <SideNavigation

            // trigger mobile side navbar
            sideNavToggle={setsidenavtoggle}
            sideNavState={sidenavtoggle}

          />

        </Grid>

        <Grid item xs={12} md={11}>

          <Table/>

        </Grid>

      </Grid>

      <Message/>

    </div>
  );
}

export default App;
