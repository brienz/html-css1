import React from 'react'
import csvIcon from '../../../img/csv.svg'

const TableNavBar = () => {
    return (
        <nav className='table-navbar'>
            
            <h1>
                pricing
            </h1>

            {/* Primary table nav links*/}
            <div className='primary-links'>

                <button>
                    SMS
                </button>

                <button>
                    Voice Standard
                </button>

                <button>
                    Voice Premium
                </button>

            </div>

            {/* Primary table nav links mobile */}
            <select>
                <option value='sms'>
                    SMS
                </option>

                <option value='voice standard'>
                    Voice Standard
                </option>

                <option value='voice premium'>
                    Voice Premium
                </option>
            </select>

            {/* convert table into csv */}
            <img
                src={csvIcon}
                alt='csv icon'
                width='40'
            />

        </nav>
    )
}

export default TableNavBar