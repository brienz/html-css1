import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

// Display each information from the data
const TableContent = ({ country, code, destination, price }) => {
    
    return (
        <TableRow className='tableBodyRow'>

            <TableCell className='bold'>
                {country}
            </TableCell>

            <TableCell
                align='center'
            >
                {code}
            </TableCell>

            <TableCell
                align='center'
            >
                {destination}
            </TableCell>

            <TableCell
                align='center'
                className='price-color'
            >
                {price}
            </TableCell>

        </TableRow>
    )
}

export default TableContent