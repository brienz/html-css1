import React, { useState } from 'react'
import { MdArrowUpward } from "react-icons/md";
import Table from '@material-ui/core/Table'
import TableContent from './TableContent'
import TableHead from '@material-ui/core/tableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'

// Table wrapper collect information and pass each info
// in table content component.
const TableWrapper = ({ information }) => {

    return (
        <Table
            className='table'
        >

            <TableHead>

                <TableRow>

                    <TableCell className='tableHeadCell text-blue'>
                        Country <MdArrowUpward/>
                    </TableCell>

                    <TableCell align='center' className='tableHeadCell'>
                        Code
                    </TableCell>

                    <TableCell align='center' className='tableHeadCell'>
                        Destination
                    </TableCell>

                    <TableCell align='center' className='tableHeadCell'>
                        Price
                    </TableCell>

                </TableRow>

            </TableHead>

            <TableBody>

                {
                    information.map((obj, i) => (

                        <TableContent
                            key={i}
                            country={obj.country}
                            code={obj.code}
                            destination={obj.destination}
                            price={obj.price}
                        />

                    )).slice(0, 7)
                }

            </TableBody>

        </Table>
    )
}

export default TableWrapper;