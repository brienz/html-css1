import React from 'react'
import TableNavBar from './components/TableNavBar'
import dummyData from '../../helper/dummyData.json'
import TableWrapper from './components/TableWrapper'
import TableContainer from '@material-ui/core/TableContainer'
import { MdChevronLeft, MdChevronRight } from "react-icons/md";

// Table page components
const Table = () => {

    return (
        <div className='tableComponent'>
           
            {/* Table   Navigation bar */}
            <TableNavBar/>

            {/* Main Table */}
            <TableContainer
                className='tableContainer'
            >

                {/* Pass dummy data to a table wrapper */}
                <TableWrapper
                    information={dummyData}
                />

                <div className='tableFooter'>

                    <p className='tableFooterPage'>
                        1-4 of 4
                    </p>

                    <MdChevronLeft className='tableFooterArrow'/>

                    <MdChevronRight className='tableFooterArrow'/>

                </div>

            </TableContainer>

            <p className='copyright'>
                &copy; 2021 Rington. All rights reserved.
            </p>
            
        </div>
    )
}

export default Table
