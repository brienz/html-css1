import React from 'react'
import { MdInsertChart, MdRecordVoiceOver, MdMessage,
        MdRingVolume, MdBook, MdSettings, MdHeadsetMic,
        MdDescription, MdEmail } from "react-icons/md";

// Dashboard Side navigation
const SideNavigation = ({ sideNavState, sideNavToggle }) => {

    const hover = (bool) => {
        sideNavToggle( bool )
    }

    return (
        <nav 
            className={ sideNavState ? 'sideNavigation' : 'sideNavigation-hide'}
            onMouseEnter={() => hover( true ) }
            onMouseLeave={() => hover( false ) }
        >
            <div>
                <MdInsertChart/> 
                <span> Dashboard </span>
            </div>

            <ul>

                <li>
                    <MdRecordVoiceOver/> 
                    <span> Voice </span>
                </li>

                <li>
                    <MdMessage/>
                    <span> SMS </span>
                </li>

                <li>
                    <MdRingVolume/>
                    <span> Numbers </span>
                </li>

            </ul>

            <div>
                <MdBook/>
                <span> Reports </span>
            </div>

            <ul>

                <li>
                    <MdSettings/>
                    <span> API </span>
                </li>

                <li>
                    <MdHeadsetMic/>
                    <span> Support </span>
                </li>

                <li>
                    <MdDescription/>
                    <span> FAQ </span>
                </li>

                <li>
                    <MdEmail/>
                    <span> Contacts </span>
                </li>

            </ul>

        </nav>
    )
}

export default SideNavigation
