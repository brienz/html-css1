import React, { useState } from 'react'
import { MdMenu, MdApps } from "react-icons/md";
import Pricing from './components/Pricing';
import Search  from './components/Search';
import Notification from './components/Notification';
import Money from './components/Money';
import User from './components/User';

//Dashboard top navigation

// Props: 
// topNavToggle props trigger the mobile top navigation to show or hide
// topNavState state of top navigation

// sideNavToggle props trigger the mobile side navigation to show or hide
// sidevState state of side navigation

const TopNavigation = ({ topNavToggle, topNavState, sideNavToggle, sideNavState, }) => {

    return (

        <>

        <div className='topnav'>

            {/* Side bar button */}
            <button 
                className='sidebar-btn'
                onClick={() => {
                    topNavToggle( false )
                    sideNavToggle( !sideNavState )
                }}
            >
                <MdApps/>
            </button>

            <h1>
                LOGO
            </h1>

            {/* Primary Navigation component */}
            <div className='primary-nav-wrapper'>

                {/* Search */}
                <Search/>


                {/* Notification */}
                <Notification/>

                <ul className='primary-nav-items'>

                    {/* Pricing */}
                    <Pricing/>


                    {/* Available money */}
                    <Money/>

                    {/* User name */}
                    <User/>

                </ul>

            </div>

            <div className='mobile-btns'>            
                <Notification/>

                {/* Top navbar button */}
                <button 
                    className='topbar-btn'
                    onClick={() => {
                        topNavToggle( !topNavState )
                        sideNavToggle( false )
                    }}
                >

                    <MdMenu/>

                </button>

            </div>


        </div>


        {/* Mobile NavBar dropdown */}
        <nav className={ topNavState ? 'mobile-dropdown-show' : 'mobile-dropdown-hide'}>
            
            <Search/>
            
            <div>
                <Pricing/>

                <Money/>
            </div>

            <User/>

        </nav>
    </>
    )
}

export default TopNavigation
