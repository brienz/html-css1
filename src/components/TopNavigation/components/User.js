import React, { useState } from 'react'
import { MdPerson, MdKeyboardArrowDown } from "react-icons/md";

// User navbar
// 
const User = () => {

    const [dropdown, setdropdown] = useState(false)

    return (
        <li className='userwrapper'>
        
            <div 
                className='user'
                onClick={() => setdropdown(!dropdown)}
            >
                <MdPerson className='add-icon'/>

                <span>
                    Name V
                </span>

                <MdKeyboardArrowDown className='add-icon'/>
            </div>

            <div className={dropdown ? 'diamond' : undefined}>
            </div>
            
            <ul className={dropdown ? 'dropdown-show' : 'dropdown-hide'}>
                <li>
                    Personal details
                </li>

                <li>
                    Company details
                </li>

                <li>
                    Payments
                </li>

                <li>
                    Change Password
                </li>
            </ul>
        
        </li>
    )
}

export default User
