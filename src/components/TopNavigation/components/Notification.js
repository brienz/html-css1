import React from 'react'
import { MdNotifications } from "react-icons/md";

// notification top navigation component
const Notification = () => {
    return (
        <div className='notification'>

            <span>
                3
            </span>

            <MdNotifications/>

        </div>
    )
}

export default Notification
