import React from 'react'
import { MdSearch } from "react-icons/md";

// top navigation Search component
const Search = () => {
    return (
        <div className='search'>
            <MdSearch/>
            <input type='text'/>
        </div>

    )
}

export default Search