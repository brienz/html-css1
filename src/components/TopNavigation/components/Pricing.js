import React from 'react'
import { MdFormatListBulleted } from "react-icons/md";

// Pricing top navigation component
const Pricing = () => {
    return (
        <li className='pricing'>

            <MdFormatListBulleted className='add-icon'/> 
            
            <span>
                Pricing
            </span>

        </li>
    )
}

export default Pricing
