import React from 'react'
import { MdAddCircle } from "react-icons/md"

// top navigation available money component
const Money = () => {
    return (
        <li className='money'>

            <span style={{ fontSize: '.60rem' }}> $ </span>

            <span> 120.60 </span> 

            <MdAddCircle className='add-icon'/>

        </li>
    )
}

export default Money
