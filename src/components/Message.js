import React from 'react'
import { MdChatBubble } from "react-icons/md";

// Floating messaging component
const Message = () => {
    return (
        <div className='messageWrapper'>
            <MdChatBubble/>
        </div>
    )
}

export default Message
